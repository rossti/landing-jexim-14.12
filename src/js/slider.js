$(document).ready(function() {
    if(window.matchMedia('(max-width: 1023px)').matches)
    {
        $('.multiple-about').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000
        });
        $('.multiple-items').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
        });
        $('.double-items').slick({
            infinite: true
        });
    } else {
        $('.multiple-about').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000
        });
        $('.multiple-items').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
        });
        $('.double-items').slick({
            infinite: true
        });
    }


});